module.exports = function(grunt) {

  grunt.initConfig({
    watch: {
      css: {
        files: ['public/css/*.css', '!public/css/*.min.css'],
        tasks: ['cssmin']
      },
      // js: {
      //   files: ['src/js/*.js'],
      //   tasks: ['uglify:dev']
      // }
    },
    responsive_images: {
      dev: {
        options: {
          engine: 'im',
          sizes: [{
            width: 500,
            quality: 50
          }]
        },
        files: [{
          expand: true,
          src: ['*.{gif,jpg,png}'],
          cwd: 'public/img/',
          dest: 'public/img/500/'
        }]
      }
    },
    babel: {
      options: {
        sourceMap: false,
        presets: ['@babel/preset-env']
      },
      dist: {
        files: [{
          expand: true,
          cwd: 'public/js',
          src: ['*.js', '!*.min.js', '!main.js'],
          dest: 'public/js',
          ext: '.es5.js'
        }]
      }
    },
    cssmin: {
      target: {
        files: [{
          expand: true,
          cwd: 'public/css',
          src: ['*.css', '!*.min.css'],
          dest: 'public/css',
          ext: '.min.css'
        }]
      }
    },
    uglify: {
      target: {
        files: [{
          expand: true,
          cwd: 'public/js',
          src: ['*.es5.js', '!*.min.js'],
          dest: 'public/js',
          ext: '.min.js'
        }]
      }
    }
  });

  grunt.loadNpmTasks('grunt-babel');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-responsive-images');

  grunt.registerTask('default', [
    'babel',
    'cssmin',
    'responsive_images',
    'uglify'
  ]);
};
