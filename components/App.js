import React, { Component } from 'react'
import ReactDOM from 'react-dom'

import { Provider } from 'react-redux'
import store from '../store'

class App extends Component {
  render() {
    return (
      <div>
        <h1>Hello,</h1>
        <p>world!</p>
      </div>
    );
  }
}

ReactDOM.render((
  <Provider store={store}>
    <App />
  </Provider>
), document.getElementById('app'));
