/**
 * Referenced for guidance:
 *  https://github.com/anagabriel42/image-serve/blob/master/app/assets/javascripts/serviceworker.js.erb
 *  https://github.com/jakearchibald/wittr/blob/task-cache-avatars/public/js/sw/index.js
 */
// Callback based library class for IndexedDB
class IDB {
  // the document's IDB needs to passed into the constructor
  // ex: self.indexedDB
  constructor(idb) {
    this.idb = idb;
  }

  // this allows users to upgrade using a callback
  // containing the database's necessary upgrades
  // you can add objectStores using the callback
  upgradeDB(db_name, version = 1, callback) {
    if (!this.idb) return null;

    this.db_name = db_name;
    this.v = version;

    let dbOpen = this.idb.open(db_name, version);

    dbOpen.onupgradeneeded = (event) => {
     let db = event.target.result;
     callback(db);
    };
   }

  // store - the name of the objectStore to access
  // data - the data to be added into the db
  putObject(store, data) {
    if (!this.idb) return null;

    let dbOpen = this.idb.open(this.db_name, this.v);

    dbOpen.onsuccess = (event) => {

      var db = dbOpen.result;
      var tx = db.transaction(store, 'readwrite');
      tx.objectStore(store).put(data);

    };
  }

  // store - the name of the objectStore to access
  // keyPath - the specific keyPath used to index the object
  deleteObject(store, keyPath) {
    if (!this.idb) return null;

    let dbOpen = this.idb.open(this.db_name, this.v);

    dbOpen.onsuccess = (event) => {
      let tx = dbOpen.result.transaction(store, 'readwrite');
      tx.objectStore(store).delete(keyPath);
    };
  }

  // store - the name of the objectStore to access
  // callback - allows you to access all of the objects
  getAll(store, callback) {
    if (!this.idb) return null;

    let dbOpen = this.idb.open(this.db_name, this.v);

    dbOpen.onsuccess = (event) => {
      let db = dbOpen.result;
      let tx = db.transaction(store, 'readwrite');
      let request = tx.objectStore(store).getAll();

      request.onsuccess = function(event) {
        let all = event.target.result;
        callback(all, dbOpen);
      };
    };
  }
}
