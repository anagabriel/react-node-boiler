function serialize(request) {
  let headers = {};
  for (let pair of request.headers.entries()) {
    headers[pair[0]] = pair[1];
  }

  let req = {
    headers: headers,
    url: request.url,
    method: request.method,
    mode: request.mode,
    credentials: request.credentials,
    cache: request.cache,
    redirect: request.redirect,
    referrer: request.referrer
  };

  let data = {
    timestamp: (new Date()).toString(),
    request: req
  };

  return request.clone().json().then((body) => {
    data.request.body = body;
    return Promise.resolve(data);
  });
}

function deserialize(data) {
  let url = new URL(data.url);

  let init = {
    headers: data.headers,
    method: data.method,
    mode: data.mode,
    credentials: data.credentials,
    cache: data.cache,
    redirect: data.redirect,
    referrer: data.referrer,
    body: JSON.stringify(data.body)
  }

  return new Request(url.pathname, init);
}
