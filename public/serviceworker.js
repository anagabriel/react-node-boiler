/**
 * Referenced for guidance:
 *  https://github.com/anagabriel42/image-serve/blob/master/app/assets/javascripts/serviceworker.js.erb
 *  https://github.com/jakearchibald/wittr/blob/task-cache-avatars/public/js/sw/index.js
 */
importScripts('/js/idb.js');
importScripts('/js/requests.js');
importScripts('/js/todo.js');

const CACHE_VERSION = 'v1';
const CACHE_NAME = 'rb-' + CACHE_VERSION;
const DB_NAME = 'rb-db-' + CACHE_VERSION;
const tables = { };

let idb = indexedDB || mozIndexedDB || webkitIndexedDB || msIndexedDB;

if (idb) {
  idb = new IDB(idb);
  idb.upgradeDB(DB_NAME, 1, (db) => {
    db.onerror = (event) => {
      console.log('Error loading database.')
    };

    db.createObjectStore('todo', { keyPath: 'timestamp' });
  });
}

self.addEventListener('install', (event) => {
  console.log('Installing sw.js');
  event.waitUntil(
    caches.open(CACHE_NAME).then(function prefill(cache) {

      for (let table in tables) {
        updateItems(table);
      }

      return cache.addAll([
        `/`,
        `/favicon.ico`,
        `/manifest.json`,
        `/offline.html`,
        `/css/style.min.css`,
        `/img/loading.gif`,
        `/img/loading.svg`,
        `/img/hamburger_small.png`,
        `/img/hamburger_small.svg`,
        `/img/hamburger_icon.png`,
        `/img/hamburger_icon.svg`,
        `/js/main.js`,
        `/js/main.map`
      ]);
    })
  );
});

self.addEventListener('activate', (event) => {
  console.log('Activating sw.js');

  event.waitUntil(
    caches.keys().then((cacheNames) => {
      return Promise.all(
        cacheNames.filter((cacheName) => {
          return cacheName.indexOf(CACHE_VERSION) === -1;
        }).map((cacheName) => {
          return caches.delete(cacheName);
        })
      );
    })
  );
});

self.addEventListener('fetch', (event) => {
  let url = new URL(event.request.url);
  if (!isPathAccepted(url)) return;

  switch (event.request.method) {
    case 'GET':
    case 'get':
      event.respondWith(servePages(event.request, url));
      return;
    case 'POST':
    case 'post':
    case 'PUT':
    case 'put':
    case 'DELETE':
    case 'delete':
      event.respondWith(update(idb, navigator, event.request));
      return;
    default:
      event.respondWith(
        fetch(event.request).catch(() => {
          return caches.match(url).then((response) => {
            if (response) return response;
            return caches.match('/offline.html');
          }).catch(() => {
            return caches.match('/offline.html');
          });
        })
      );
    break;
  }
});

function servePages(request, url) {
  return caches.open(CACHE_NAME).then((cache) => {
    return cache.match(url).then((response) => {
      if (response) return response;

      return fetch(request).then((networkResponse) => {
        cache.put(url, networkResponse.clone());
        return networkResponse;
      }).catch(() => {
        return caches.match('/offline.html');
      });
    });
  });
}

self.addEventListener('online', () => {
  console.log('dequeue');
  idb.getAll('todo', (requests, dbOpen) => {
    for (let r of requests) {
      let request = deserialize(r.request);
      console.log(request);

      fetch(request).then((response) => {
        if (!response) return;

        let tx = dbOpen.result.transaction('requests', 'readwrite');
        tx.objectStore('requests').delete(r.timestamp);

        console.log('Deleted', r);

      }).catch((err) => { console.log(err) });
    }
  });
});

function isPathAccepted(url) {
  if (contains(url, 'maps') || contains(url, 'stripe')) return false;
  let path = url.pathname.substr(1);
  if (tables[path] || contains(path, 'register')) return false;
  return true;
}

function contains(s1, s2) {
  return s1.toString().indexOf(s2.toString()) !== -1;
}

function updateItems(path) {
  fetch('/' + path)
  .then(res => res.json())
  .then((data) => {
    if (!data.success) return;
    data.result.forEach((item) => {
      idb.putObject(path, item);
    });
  });
}
