import { combineReducers, createStore } from 'redux'

import example from './example'

export default createStore(combineReducers({ example }))
