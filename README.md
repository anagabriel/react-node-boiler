# react-node-boiler
boilerplate for all of my Node.js and ReactJS projects

## to use
```bash
$ git clone https://github.com/anagabriel42/react-node-boiler.git
$ cd react-node-boiler
$ git push --mirror <your repo>
```

## to edit
* README.md
### public
* manifest.json
* favicon.ico
* serviceworker.js
### views
* index.hjs

## to build
```bash
$ npm i
$ npm run build
$ grunt
$ npm start
```

## to watch files
```bash
$ npm run watch
$ grunt watch
```
